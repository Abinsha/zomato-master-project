import axios from 'axios'
import { API_URL } from '../../../key'

//Redux types
import { GET_IMAGE } from './Image.type'

export const getImage = (_id)  => async (dispatch) => {
    try {
        const Image = await axios({
            method: "GET",
            url: `${API_URL}/image/${_id}`
        })

        return dispatch({ type: GET_IMAGE, payload: Image.data})
    } catch (error) {
        return dispatch({ type: "ERROR", payload: error})
    }
}